export const getTwoDimensionalArray = (number) => {
  const twoDimensionalArray = new Array(number);

  for (let i = 0; i < twoDimensionalArray.length; i++) {
    twoDimensionalArray[i] = new Array(number);

    for (let j = 0; j < twoDimensionalArray[i].length; j++) {
      twoDimensionalArray[i][j] = `${i}${j}`
    }
  }

  return twoDimensionalArray;
}