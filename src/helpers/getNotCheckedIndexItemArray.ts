export const getNotCheckedIndexItemArray = (array: any[]) => {
  let randomIndex = Math.floor(Math.random() * array.length);

  while (array[randomIndex] === "") {
    randomIndex = Math.floor(Math.random() * array.length);
  }

  return randomIndex
}
