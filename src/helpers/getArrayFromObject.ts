import { getNameFromKeyObject } from "$helpers/getNameFromKeyObject";

export const getArrayFromObject = data => Object.keys(data)
  .map(key => ({ id: key, label: getNameFromKeyObject(key), ...data[key] }));