export const getNameFromKeyObject = key =>
  key.split(/(?=[A-Z])/).join(" ").toLowerCase();
