import React, { Suspense } from "react";
import { Redirect, Route, Switch, BrowserRouter } from "react-router-dom";

import { MainLayout } from "$layouts/MainLayout";

import { PAGES, URL } from "$constants/navigation";

export const App = () => (
  <BrowserRouter>
    <MainLayout>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          {PAGES.map(({ path, component }) => (
            <Route
              exact
              key={path}
              path={path}
              component={component}
            />
          ))}
          <Redirect to={URL.HOME} />
        </Switch>
      </Suspense>
    </MainLayout>
  </BrowserRouter>
);