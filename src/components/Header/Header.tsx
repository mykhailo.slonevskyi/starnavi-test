import React from "react";

import { Box } from "$UI/Box";
import { Grid } from "$UI/Grid";
import { AppBar } from "$UI/AppBar";

import { MainNav } from "$components/MainNav";

export const Header: React.FC = () => {

  return (
    <AppBar
      color="default"
      position="static"
    >
      <Box
        paddingY={2}
        paddingX={1}
      >
        <Grid container>
          <Grid item xs={4}>
            <MainNav />
          </Grid>
        </Grid>
      </Box>
    </AppBar>
  );
};
