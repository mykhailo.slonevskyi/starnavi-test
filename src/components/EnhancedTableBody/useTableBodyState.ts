import React from "react";

import { TableState } from "./constants"

function useTableBodyState(childrenQty: number, isLoading?: boolean) {
  const [state, setState] = React.useState(TableState.Empty);

  React.useEffect(() => {
    switch (true) {
      case childrenQty === 0:
        setState(TableState.Empty);
        break;
      default:
        setState(TableState.Default);
    }
  }, [isLoading, childrenQty]);

  return state;
}

export {
  useTableBodyState,
};
