import React from "react";

import { TableRow } from "$UI/TableRow";
import { TableBody } from "$UI/TableBody";
import { TableCell } from "$UI/TableCell";

import { useTableBodyState } from "./useTableBodyState";

import { useStyles } from "./styles";

import { IProps } from "./types";
import { TableState } from "./constants"

const EnhancedTableBody: React.FC<IProps> = props => {
  const classes = useStyles();
  const tableState = useTableBodyState(
    React.Children.toArray(props.children).length
  );

  return (
    <>
      {tableState === TableState.Empty && (
        <TableBody innerRef={props.innerRef}>
          <TableRow className={classes.rowHeight}>
            <TableCell
              align="center"
              colSpan={props.colSpan + 2}
            >
              There are not winners.
            </TableCell>
          </TableRow>
        </TableBody>
      )}
      {tableState === TableState.Default && (
        <TableBody innerRef={props.innerRef}>
          {props.children}
        </TableBody>
      )}
    </>
  );
};

export {
  EnhancedTableBody,
};
