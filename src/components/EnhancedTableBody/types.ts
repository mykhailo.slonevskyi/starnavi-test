export interface IProps {
  colSpan: number;
  innerRef?: (element: HTMLElement | null) => any;
}
