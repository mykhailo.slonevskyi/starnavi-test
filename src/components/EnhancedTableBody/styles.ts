import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  rowHeight: {
    height: 50,
  },
}));
