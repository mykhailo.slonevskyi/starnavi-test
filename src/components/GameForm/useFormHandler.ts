import pipe from "ramda/src/pipe";
import merge from "ramda/src/merge";

import { useEffect } from "react";
import { stringify } from "query-string";
import { useForm } from "react-hook-form";
import { useHistory, useLocation } from "react-router-dom";

import { NAME, MODE } from "$constants/strings";

const handleFormData = (formData: any) => {
  return merge(formData, {
    name: formData.name,
    mode: formData.mode.id,
  })
}

const useQuery = () => {
  return new URLSearchParams(useLocation().search);
}

export function useFormHandler() {
  const query = useQuery();
  const { push } = useHistory();
  const location = useLocation();
  const formMethods = useForm();

  useEffect(() => {
    if(query.get(NAME) || query.get(MODE)) {
      return
    }
    const name = query.get(NAME);
    const mode = query.get(MODE);

    formMethods.setValue(NAME, name)
    formMethods.setValue(MODE, mode)

  }, [location])

  const handleSubmit = pipe(
    handleFormData,
    stringify,
    search => push(`?${search}&${Math.random()}`)
  )

  return ({
    formMethods,
    handleSubmit
  })
}