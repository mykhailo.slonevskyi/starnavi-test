import compose from "ramda/src/compose";

import React from "react";

import { InputController } from "$components/InputController";
import { GameModeSelect } from "$components/GameModeSelect";

import { Grid } from "$UI/Grid";
import { Button } from "$UI/Button";
import { TextField } from "$UI/TextField";

import { MODE, USER_NAME, NAME, PICK_GAME_MODE } from "$constants/strings";

import { useFormHandler } from "./useFormHandler";
import { useGameContext } from "$components/Game/context";

export const GameForm: React.FC = () => {
  const { formMethods, handleSubmit } = useFormHandler();
  const { gameStatus, handleGameStatusStarted } = useGameContext();

  return (
    <Grid
      container
      spacing={2}
      component="form"
      alignItems="center"
      onSubmit={compose(
        handleGameStatusStarted,
        formMethods.handleSubmit(handleSubmit as any))
      }
    >
      <Grid item xs={4}>
        <InputController
          required
          name={MODE}
          control={formMethods.control}
          label={PICK_GAME_MODE}
          as={GameModeSelect}
        />
      </Grid>
      <Grid item xs={4}>
        <InputController
          required
          name={NAME}
          as={TextField}
          control={formMethods.control}
          label={USER_NAME}
        />
      </Grid>
      <Grid item xs={4}>
        <Button
          fullWidth
          type="submit"
          color="primary"
          variant="outlined"
        >
          {gameStatus === "notStarted" && (
            "Play"
          )}
          {gameStatus !== "notStarted" && (
            "Play again"
          )}
        </Button>
      </Grid>
    </Grid>
  )
}
