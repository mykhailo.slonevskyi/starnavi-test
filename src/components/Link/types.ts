import { LinkProps } from "@material-ui/core/Link";

export interface IProps extends LinkProps {
  to?: string;
  component?: string;
}
