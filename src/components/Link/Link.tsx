import React from "react";
import MaterialLink from "@material-ui/core/Link";

import { ExternalLink } from "$UI/ExternalLink";
import { RouterLink } from "$UI/RouterLink";

import { IProps } from "./types";

export const Link: React.FC<IProps> = React.memo(props => {
  if (props.to) {
    return <RouterLink {...props as any} />;
  }

  if (props.href) {
    return <ExternalLink {...props as any} />;
  }

  return <MaterialLink {...props} />;
});
