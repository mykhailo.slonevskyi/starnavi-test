import React from "react";

import { GameForm } from "$components/GameForm";
import { GameField } from "$components/GameField";

import { useGameContext } from "./context";

export const Game: React.FC = () => {
  const { getGameModes } = useGameContext();

  React.useEffect(() => {
    getGameModes();
  }, [])

  return (
    <>
      <GameForm />
      <GameField />
    </>
  )
}