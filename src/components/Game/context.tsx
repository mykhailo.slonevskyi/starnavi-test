import React, { createContext, useContext } from "react";

import { useContextValue } from "./useContextValue";

// import { IGameContext } from "./types";

const GameContext = createContext<any | {}>({});

export const GameProvider: React.FC = props => {
  const { value } = useContextValue()

  return (
    <GameContext.Provider {...props} value={value} />
  )
}

export function useGameContext() {
  const context = useContext(GameContext);

  if (!context) {
    throw new Error("useGameContext must be inside GameContext");
  }

  return context;
}
