import { useState, useMemo } from "react";

import axios from "$utils/API";

import { getArrayFromObject } from "$helpers/getArrayFromObject";

import { IGameMode } from "./types";

export function useContextValue() {
  const [gameModes, setGameModes] = useState<IGameMode[] | []>([]);
  const [gameStatus, setGameStatus] = useState<"notStarted" | "started" |  "finished">("notStarted");

  const getGameModes = async () => {
    await axios.get("/game-settings")
      .then(response => getArrayFromObject(response.data))
      .then(data => {
        setGameModes(data)
      })
      .catch(error => console.log(error));
  }

  const handleGameStatusStarted = () => {
    setGameStatus("started")
  }
  const handleGameStatusFinished = () => {
    setGameStatus("finished")
  }
  const handleGameStatusNotStarted = () => {
    setGameStatus("notStarted")
  }

  const value = useMemo(() => ({
    gameModes,
    gameStatus,
    getGameModes,
    handleGameStatusStarted,
    handleGameStatusFinished,
    handleGameStatusNotStarted,
  }), [gameModes, gameStatus])

  return ({
    value
  })
}