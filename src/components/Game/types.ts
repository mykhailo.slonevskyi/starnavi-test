

export interface IGameMode {
  id: string;
  label: string;
  field: number;
  delay: number;
}

export interface IGameFormContext {
  gameModes: IGameMode[];
}

export interface IFormData {
  // departureCity: City
  // arrivalCity: City
  // departureDate: Date
  // returnDate?: Date
  // qty: number
}
