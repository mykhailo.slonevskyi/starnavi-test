import includes from "ramda/src/includes";

import React from "react";
import { AutocompleteRenderInputParams } from "@material-ui/lab/Autocomplete";
import { TextFieldProps } from "@material-ui/core/TextField";

import { Autocomplete } from "$components/Autocomplete";
import { useGameContext } from "$components/Game/context";

import { TextField } from "$UI/TextField";

import { PICK_GAME_MODE } from "$constants/strings";

export const GameModeSelect: React.FC<TextFieldProps> = props => {
  const { gameModes } = useGameContext();

  const getOptionLabel = value => value.label;

  const value = includes(props.value, gameModes)
    ? props.value
    : null;

  //TODO remove any
  const handleChange = (e: React.ChangeEvent, newValue: any) => {
    props.onChange && props.onChange(newValue as any);
  }

  function renderInput(params: AutocompleteRenderInputParams) {
    return (
      <TextField
        {...params}
        name={props.name}
        size={props.size}
        label={props.label}
        variant={props.variant}
        required={props.required}
        placeholder={PICK_GAME_MODE}
        InputProps={{
          ...params.InputProps,
          ...props.InputProps,
        }}
      />
    );
  }

  return (
    <Autocomplete
      autoSelect
      value={value}
      onChange={handleChange}
      renderInput={renderInput}
      getOptionLabel={getOptionLabel}
      options={gameModes}
      getOptionSelected={getOptionSelected}
    />
  );
};

function getOptionSelected(option: any, value: any) {
  return value && option.id === value.id
}
