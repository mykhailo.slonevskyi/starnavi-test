import React from "react";
import { Controller, ControllerProps } from "react-hook-form";

const InputController: React.FC<ControllerProps<any>> = props => (
  <Controller
    {...props}
    /**
     * We need defaultValue form to fix the error. More:
     * https://github.com/react-hook-form/react-hook-form/issues/802
     */
    defaultValue={props.defaultValue ?? ""}
  />
);

export {
  InputController,
};
