import React from "react";

import { Box } from "$UI/Box";
import { Typography } from "$UI/Typography";

import { IProps } from "./types";

export const PageTitle: React.FC<IProps> = props => {

  return (
    <>
      <Box paddingY={3} mb={5}>
        <Typography
          variant="h4"
          align="center"
        >
          {props.title}
        </Typography>
      </Box>
      {props.children}
    </>
  );
};
