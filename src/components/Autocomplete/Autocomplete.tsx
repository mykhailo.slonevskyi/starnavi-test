import React from "react";
import MUIAutocomplete, { AutocompleteProps } from "@material-ui/lab/Autocomplete";

import { LOADING, EMPTY } from "$constants/strings";

export const Autocomplete: React.FC<AutocompleteProps<unknown, undefined, undefined, undefined>> = props => {

  return (
    <MUIAutocomplete
      loadingText={LOADING}
      noOptionsText={EMPTY}
      {...props}
    />
  );
};
