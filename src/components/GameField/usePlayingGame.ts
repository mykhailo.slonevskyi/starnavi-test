import { useState, useEffect } from "react";

import { useGameOptions } from "./useGameOptions";

export const usePlayingGame = () => {
  const [totalPoints, setTotalPoints] = useState<number | null>(null);

  const { gameOptions } = useGameOptions();

  useEffect(() => {
    if (!gameOptions) {
      return
    }

    setTotalPoints(gameOptions.field ** 2);
    
  }, [gameOptions])

  return ({
    totalPoints 
  })
}