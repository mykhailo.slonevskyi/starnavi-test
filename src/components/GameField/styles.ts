import { makeStyles, Theme } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme: Theme) => ({
  container: {
    maxWidth: "420px",
    margin: "0 auto",
  },
  containerField: {
    border: "1px solid #000",
    "&:after": {
      content: "''",
      display: "block",
      position: "absolute",
      paddingBottom: "100%",
    }
  },
  row: {
    display: "flex",
  },

  fieldItem: {
    display: "block",
    border: "1px solid #000",
    width: "20%",
    height: "20%",
    "&:after": {
      content: "''",
      display: "block",
      paddingBottom: "100%",
    }
  },
  fieldItemComputer: {
    backgroundColor: theme.palette.error.main,
  },
  fieldItemUser: {
    backgroundColor: theme.palette.success.main,
  },
  fieldItemPrepare: {
    backgroundColor: theme.palette.info.main,
  },
}));
