import { useState } from "react";

export const usePointsPlayers = () => {
  const [userPoints, setUserPoints] = useState(0);
  const [computerPoints, setComputerPoints] = useState(0);

  const addComputerPoint = () => {
    setComputerPoints(computerPoints => computerPoints + 1)
  }
  
  const addUserPoint = () => {
    setUserPoints(userPoints => userPoints + 1)
  }

  const clearPoints = () => {
    setUserPoints(0);
    setComputerPoints(0);
  }

  return ({
    userPoints,
    clearPoints,
    addUserPoint,
    computerPoints,
    addComputerPoint,
  })
}