import pipe from "ramda/src/pipe";
import find from "ramda/src/find";
import propEq from "ramda/src/propEq";

import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

import { ID, MODE, NAME } from "$constants/strings";
import { IGameMode } from "$components/GameForm/types";
import { useGameContext } from "$components/Game/context";

const useQuery = () => {
  return new URLSearchParams(useLocation().search);
}

export const useGameOptions = () => {
  const [gameOptions, setGameOptions] = useState<IGameMode | null>(null);
  const [userName, setUserName] = useState<string | null>(null);
  
  const query = useQuery();
  const location = useLocation();
  const { gameModes } = useGameContext();
  
  const currentMode = query.get(MODE);

  useEffect(() => {
    if (!currentMode) {
      return
    }
    
    setUserName(query.get(NAME))
    
    pipe(
      find(propEq(ID, currentMode)),
      value => setGameOptions(value as any)
    )(gameModes);
  }, [gameModes, location])

  return ({
    gameOptions,
    userName
  })
}