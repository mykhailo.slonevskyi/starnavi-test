import clsx from "clsx";
import dayjs from "dayjs";

import React from "react";
import { useLocation } from "react-router-dom";

import axios from "$utils/API";

import { WINNERS, COMPUTER } from "$constants/strings";

import { Box } from "$UI/Box";
import { Typography } from "$UI/Typography";

import { useGameContext } from "$components/Game/context";

import { getIndexFieldItem } from "$helpers/getIndexFieldItem";
import { getNotCheckedIndexItemArray } from "$helpers/getNotCheckedIndexItemArray";

import { useGameOptions } from "./useGameOptions";
import { usePlayingGame } from "./usePlayingGame";
import { usePointsPlayers } from "./usePointsPlayers";
import { useGameFieldHandler } from "./useGameFieldHandler";

import { useStyles } from "./styles";

export const GameField: React.FC = () => {
  const classes = useStyles();
  const refs = React.useRef([] as any);
  const { gameOptions, userName } = useGameOptions();
  const { getField, fieldItems, playingFieldArray, checkedPlayingFieldArrayItem } = useGameFieldHandler();
  const { totalPoints } = usePlayingGame();
  const { gameStatus, handleGameStatusFinished } = useGameContext();
  const {
    userPoints,
    clearPoints,
    addUserPoint,
    computerPoints,
    addComputerPoint,
  } = usePointsPlayers();
  const location = useLocation();
  
  const [isChangeItem, setIsChangeItem] = React.useState(false);
  const [gameFieldStatus, setGameFieldStatus] = React.useState(false);
  const [winner, setWinner] = React.useState("");

  const handleIsChangeItem = (isChange: boolean) => {
    setIsChangeItem(isChange);
  }

  React.useEffect(() => {
    refs.current = []
  }, [])

  // clear field items
  React.useEffect(() => {
    for (let i = 0; i < playingFieldArray.length; i++) {
      refs.current[i].className = classes.fieldItem;
    }
    setGameFieldStatus(false)
  }, [location])

  // starting game
  React.useEffect(() => {
    if (gameStatus !== "started") {
      return
    }
    
    getField();
    clearPoints();

    setTimeout(() => {
      setGameFieldStatus(true)
    }, 1000)

  }, [gameStatus, gameOptions, location])

  React.useEffect(() => {
    if (!totalPoints) {
      return
    }
    if (userPoints > Math.floor(totalPoints/2)) {
      setGameWinner(userName ? userName : "")
      setWinner(userName ? userName : "")
      setGameFieldStatus(false)
      return
    }
    if (computerPoints > Math.floor(totalPoints/2)) {
      setGameWinner(COMPUTER)
      setWinner(COMPUTER)
      setGameFieldStatus(false)
      return
    }
  }, [userPoints, computerPoints])

  React.useEffect(() => {
    if (!gameOptions || !gameFieldStatus) {
      return
    }

    for (let i = 0; i < playingFieldArray.length; i++) {
      refs.current[i].disabled = true;
    }

    const fieldArrayItem = getNotCheckedIndexItemArray(playingFieldArray);
    checkedPlayingFieldArrayItem(fieldArrayItem)

    // Switch button for clicking
    const currentButton = refs.current[fieldArrayItem];
    currentButton.disabled = false;
    currentButton.className = clsx(classes.fieldItemPrepare, classes.fieldItem);

    // If user didn't click, then computer get point
    const switchTimer = setTimeout(() => {
      currentButton.disabled = true;
      currentButton.className = clsx(classes.fieldItemComputer, classes.fieldItem);
      handleIsChangeItem(!isChangeItem);
      addComputerPoint();
    }, gameOptions.delay)

    return () => clearTimeout(switchTimer);

  }, [isChangeItem, gameFieldStatus])

  const setGameWinner = async (winner: string) => await axios({
    method: 'post',
    url: `/${WINNERS}`,
    data: {
      winner: winner,
      date: dayjs().format("HH:mm; DD MMMM YYYY"),
    }
  }).then(handleGameStatusFinished)
    .catch(error => console.log(error));


  const onClickUser = (currentButton) => {
    currentButton.disabled = true;
    currentButton.className = clsx(classes.fieldItemUser, classes.fieldItem);
    handleIsChangeItem(!isChangeItem);
    addUserPoint();
  }

  return (
    <div className={classes.container}>
      <Box marginY={4}>
        <Typography align="center" variant="h6">
          {gameStatus === "started" && gameOptions?.label}
          {gameStatus === "finished" && (
            `winner is ${winner}!`
          )}
        </Typography>
      </Box>
      <Box className={clsx(fieldItems.length && classes.containerField)}>
        {fieldItems.map((row, indexRow) => (
          <div key={row} className={classes.row}>
            {row.map((elem, indexElem) => {
              return (
                <button
                  key={elem}
                  ref={el => refs.current[getIndexFieldItem(fieldItems.length, indexRow, indexElem)] = el}
                  className={classes.fieldItem}
                  onClick={() => onClickUser(refs.current[getIndexFieldItem(fieldItems.length, indexRow, indexElem)])}
                />
              )
            })}
          </div>
        ))}
      </Box>
    </div>
  )
}
