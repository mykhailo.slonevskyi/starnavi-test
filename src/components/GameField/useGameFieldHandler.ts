import remove from "ramda/src/remove";
import insert from "ramda/src/insert";
import flatten from "ramda/src/flatten";

import { useState, useEffect } from "react";

import { getTwoDimensionalArray } from "$helpers/getTwoDimensionalArray";

import { useGameOptions } from "./useGameOptions";

export const useGameFieldHandler = () => {
  const { gameOptions } = useGameOptions();
  const [fieldItems, setFieldItems] = useState<any[]>([]); //For render components
  const [playingFieldArray, setPlayingFieldArrays] = useState<String[]>([]); //For play game

  useEffect(() => {
    getField();
  }, [gameOptions])

  const getField = () => {
    if (!gameOptions) {
      return
    }
    
    setFieldItems(getTwoDimensionalArray(gameOptions.field));
    setPlayingFieldArrays(flatten(getTwoDimensionalArray(gameOptions.field)));
  }

  const checkedPlayingFieldArrayItem = (playingFieldArrayItemIndex: number) => {
    setPlayingFieldArrays(
      insert(playingFieldArrayItemIndex, "",
        remove(playingFieldArrayItemIndex, 1, playingFieldArray))
    )
  }

  return ({
    getField,
    fieldItems,
    playingFieldArray,
    checkedPlayingFieldArrayItem,
  })

}