export interface IGameFieldContext {
  userPoints: number;
  computerPoints: number;
}