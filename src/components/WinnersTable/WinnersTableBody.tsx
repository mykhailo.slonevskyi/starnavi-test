import React, { useEffect } from "react";

import { EnhancedTableBody } from "$components/EnhancedTableBody";
import { useGameContext } from "$components/Game/context"; 

import { WinnersTableRow } from "./WinnersTableRow";

import { useWinnersList } from "./useWinnersList";

export const WinnersTableBody = () => {
  const { winnersList, getWinnersList } = useWinnersList();
  const { gameStatus } = useGameContext()

  useEffect(() => {
    getWinnersList();
  }, [])

  useEffect(() => {
    if (gameStatus !== "finished") {
      return
    }
    getWinnersList();
  }, [gameStatus])

  return (
    <EnhancedTableBody colSpan={3}>
      {winnersList.map((winner, idx) => (
        <WinnersTableRow
          idx={idx}
          key={idx}
          {...winner}
        />
      ))}
    </EnhancedTableBody>
  );
};
