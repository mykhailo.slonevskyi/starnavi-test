import React from "react";

import { TableRow } from "$UI/TableRow";
import { TableCell } from "$UI/TableCell";

import { IWinnersTableRowProps } from "./types";

export const WinnersTableRow: React.FC<IWinnersTableRowProps> = props => (
  <TableRow>
    <TableCell>
      {props.idx}
    </TableCell>
    <TableCell>
      {props.winner}
    </TableCell>
    <TableCell>
      {props.date}
    </TableCell>
  </TableRow>
);
