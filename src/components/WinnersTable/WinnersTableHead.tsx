import React from "react";

import { TableRow } from "$UI/TableRow";
import { TableHead } from "$UI/TableHead";
import { TableCell } from "$UI/TableCell";

import { useStyles } from "./styles";

export const WinnersTableHead = () => {
  const classes = useStyles();

  const headCells = [
    { id: "winners", label: "Winners" },
    { id: "date", label: "date" },
  ]

  return (
    <TableHead>
      <TableRow>
        <TableCell align="justify" className={classes.numberCell}>
          #
        </TableCell>
        {headCells.map(({ id, label }) => (
          <TableCell key={id} align="justify">
            {label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};
