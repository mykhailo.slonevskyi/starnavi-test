import React from "react";

import { Box } from "$UI/Box";
import { Table } from "$UI/Table";
import { Typography } from "$UI/Typography";

import { WinnersTableHead } from "./WinnersTableHead";
import { WinnersTableBody } from "./WinnersTableBody";

import { useStyles } from "./styles"

export const WinnersTable = () => {
  const classes = useStyles();

  return (
    <>
      <Typography
        gutterBottom
        variant="h6"
      >
        Leader Board
      </Typography>
      <Box maxHeight="425px" overflow="auto">
        <Table className={classes.tableContainer}>
          <WinnersTableHead />
          <WinnersTableBody />
        </Table>
      </Box>
    </>
  );
};
