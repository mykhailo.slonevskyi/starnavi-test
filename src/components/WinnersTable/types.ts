export interface IWinnersTableRowProps {
  idx?: number;
  date: string;
  winner: string;
}