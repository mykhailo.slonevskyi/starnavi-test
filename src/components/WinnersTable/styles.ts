import { makeStyles } from "@material-ui/core/styles";

import { TABLE_NUMBER_WIDTH_MULTIPLIER } from "$constants/styles";

export const useStyles = makeStyles(theme => ({
  tableContainer: {
    overflow: "scroll",
    maxHeight: 200
  },

  numberCell: {
    width: theme.spacing(TABLE_NUMBER_WIDTH_MULTIPLIER),
  },
}));
