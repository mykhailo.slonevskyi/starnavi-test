import { useState } from "react";

import { WINNERS } from "$constants/strings";

import axios from "$utils/API";

export const useWinnersList = () => {
  const [winnersList, setWinnersList] = useState([]);

  const getWinnersList = async () => {
    await axios.get(`/${WINNERS}`)
      .then(response => setWinnersList(response.data))
      .catch(error => console.log(error));
  }

  return ({
    winnersList,
    getWinnersList,
  })

}