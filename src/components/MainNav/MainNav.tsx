import React from "react";

import { Link } from "$components/Link";

import { Grid } from "$UI/Grid";

import { PAGES } from "$constants/navigation";

export const MainNav = () => {

  return (
    <Grid container spacing={2}>
      {PAGES.map(page => page.label &&(
        <Grid
          item
          key={page.label}
        >
          <Link to={page.path}>
            {page.label}
          </Link>
        </Grid>
      ))}
    </Grid>
  )
};