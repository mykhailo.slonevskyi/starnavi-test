import React from "react";

import { Header } from "$components/Header";
import { PageTitle } from "$components/PageTitle";

import { Box } from "$UI/Box"

import { IProps } from "./types";
import { useStyles } from "./styles";

export const PageLayout: React.FC<IProps> = props => {
  const classes = useStyles();


  return (
    <>
      <Header />
      <main className={classes.main}>
        <PageTitle title={props.title}>
          {props.children}
        </PageTitle>
      </main>
    </>
  );
};
