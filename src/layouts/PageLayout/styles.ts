import { makeStyles, Theme } from "@material-ui/core/styles";

import { CONTENT_MAX_WIDTH } from "$constants/styles";

export const useStyles = makeStyles((theme: Theme) => ({
  main: ({
    flexGrow: 1,
    marginLeft: "auto",
    marginRight: "auto",
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    maxWidth: CONTENT_MAX_WIDTH,
  }),
}));
