import React from "react";

import { Grid } from "$UI/Grid";

import { Game } from "$components/Game";
import { WinnersTable } from "$components/WinnersTable";
import { GameProvider } from "$components/Game/context";

import { PageLayout } from "$layouts/PageLayout";

export const HomePage: React.FC = () => {

  return (
    <PageLayout title="Hello, dear User!" >
      <GameProvider>
        {/*Fix material ui horizontal scroll https://material-ui.com/components/grid/#negative-margin*/}
        <div style={{ padding: 16 }}>
          <Grid container spacing={4}>
            <Grid
              item
              xs={12}
              md={8}
            >
              <Game />
            </Grid>
            <Grid
              item
              xs={12}
              md={4}
            >
              <WinnersTable />
            </Grid>
          </Grid>
        </div>
      </GameProvider>
    </PageLayout>
  );
};
