import MaterialTableHead, { TableHeadProps } from "@material-ui/core/TableHead";
import React, { FC } from "react";

export const TableHead: FC<TableHeadProps> = props => (
  <MaterialTableHead {...props} />
);
