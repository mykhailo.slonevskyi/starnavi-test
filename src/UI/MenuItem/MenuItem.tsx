import React from "react";
import MaterialMenuItem, { MenuItemProps } from "@material-ui/core/MenuItem";

export const MenuItem: React.FC<MenuItemProps> = (props) => (
  <MaterialMenuItem {...props as any} />
);
