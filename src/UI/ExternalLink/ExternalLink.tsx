import React from "react";
import Link from "@material-ui/core/Link";

import { IProps } from "./types";

export const ExternalLink: React.FC<IProps> = props => (
  <Link
    target="_blank"
    rel="nofollow noopener"
    {...props}
  >
    {props.children}
  </Link>
);
