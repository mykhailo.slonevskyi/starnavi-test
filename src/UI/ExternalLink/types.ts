export interface IProps {
  className?: string;
  href: string;
}
