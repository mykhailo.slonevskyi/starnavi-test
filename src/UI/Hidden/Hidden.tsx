import React, { FC } from "react";

import MaterialHidden, { HiddenProps } from "@material-ui/core/Hidden";

export const Hidden: FC<HiddenProps> = props => (
  <MaterialHidden {...props}>
    {props.children}
  </MaterialHidden>
);
