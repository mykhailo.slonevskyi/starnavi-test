import MaterialTableBody, { TableBodyProps } from "@material-ui/core/TableBody";
import React, { FC } from "react";

export const TableBody: FC<TableBodyProps> = props => (
  <MaterialTableBody {...props} />
);
