import React, { FC } from "react";
import MaterialAppBar, { AppBarProps } from "@material-ui/core/AppBar";

export const AppBar: FC<AppBarProps> = props => (
  <MaterialAppBar {...props} />
);
