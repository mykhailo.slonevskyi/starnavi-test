import MaterialTypography, { TypographyProps } from "@material-ui/core/Typography";
import React, { FC } from "react";

export const Typography: FC<TypographyProps> = props => (
  <MaterialTypography {...props} />
);
