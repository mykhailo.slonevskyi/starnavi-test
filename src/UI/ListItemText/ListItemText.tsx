import React from "react";
import MaterialListItemText, { ListItemTextProps } from "@material-ui/core/ListItemText";

import { IProps } from "./types";

export const ListItemText: React.FC<IProps & ListItemTextProps> = (props) => (
  <MaterialListItemText {...props} />
);
