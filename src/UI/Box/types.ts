import { BoxProps, TypographyProps } from "@material-ui/core";

export interface IProps extends BoxProps {
  align?: TypographyProps["align"];
}
