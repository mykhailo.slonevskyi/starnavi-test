import React from "react";
import MaterialBox from "@material-ui/core/Box";

import { IProps } from "./types";

export const Box: React.FC<IProps> = (props) => (
  <MaterialBox {...props} />
);
