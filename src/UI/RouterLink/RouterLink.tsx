import clsx from "clsx";
import React from "react";

import { NavLink } from "react-router-dom";

import { IProps } from "./types";

import { useStyles } from "./styles";

export const RouterLink: React.FC<IProps> = props => {
  const classes = useStyles();

  return (
    <NavLink
      exact
      {...props}
      className={clsx(classes.link, props.className)}
    >
      {props.children}
    </NavLink>
  );
};
