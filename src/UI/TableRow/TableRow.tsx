import MaterialTableRow, { TableRowProps } from "@material-ui/core/TableRow";
import React, { FC } from "react";

export const TableRow: FC<TableRowProps> = props => (
  <MaterialTableRow {...props} />
);
