import React from "react";
// import MaterialSelect, { SelectProps } from "@material-ui/core/Select";
import MaterialSelect, { SelectProps } from "@appgeist/react-select-material-ui";

export const Select: React.FC<SelectProps> = (props) => (
  <MaterialSelect {...props as any} />
);
