import React from "react";
import MaterialTextField, { TextFieldProps } from "@material-ui/core/TextField";

export const TextField: React.FC<TextFieldProps> = props => {

  return (
    <MaterialTextField
      {...props}
      InputProps={props.InputProps}
    />
  );
};

TextField.defaultProps = {
  fullWidth: true,
  variant: "filled",
};
