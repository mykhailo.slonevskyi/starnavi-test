import React, { FC } from "react";
import MaterialButton, { ButtonProps } from "@material-ui/core/Button";

export const Button: FC<ButtonProps> = props => {

  return (
    <MaterialButton {...props} />
  );
};
