import MaterialTableCell, { TableCellProps } from "@material-ui/core/TableCell";
import React, { FC } from "react";

export const TableCell: FC<TableCellProps> = props => (
  <MaterialTableCell {...props} />
);
