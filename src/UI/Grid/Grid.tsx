import MaterialGrid from "@material-ui/core/Grid";
import React from "react";

import { IProps } from "./types";

export const Grid: React.FC<IProps> = React.memo((props) => (
  <MaterialGrid {...props} />
));
