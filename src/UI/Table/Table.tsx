import MaterialTable, { TableProps } from "@material-ui/core/Table";
import React, { FC } from "react";

export const Table: FC<TableProps> = props => (
  <MaterialTable {...props} />
);
