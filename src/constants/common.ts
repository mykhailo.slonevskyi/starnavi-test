export const API_URL = "https://starnavi-frontend-test-task.herokuapp.com/";

export const DATE_VALUE_FORMAT = "DD-MM-YYYY";

export const MAX_FIELD_ITEMS = 225;
