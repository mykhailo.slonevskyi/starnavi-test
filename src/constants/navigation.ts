import { lazy } from "react";

const HomePage = lazy(() => import("$routes/HomePage"));

export enum URL {
  HOME = "/",
}

export const PAGES = [
  { component: HomePage, path: URL.HOME, label: "Home" },
];
