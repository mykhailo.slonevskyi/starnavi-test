'use strict';
const path = require('path');
const fs = require('fs');

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebook/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
  appTsConfig: resolveApp('tsconfig.json'),
  appBuild: resolveApp('build'),
  appSrc: resolveApp('src'),
  appHtml: resolveApp('public/index.html'),
  appPublic: resolveApp('public'),
};