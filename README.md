# STARNAVI-TEST

[![node](https://img.shields.io/badge/node-v12.8.0-green.svg)](https://nodejs.org/en/)
[![yarn](https://img.shields.io/badge/yarn-1.22.4-blue.svg)](https://yarnpkg.com/lang/en/)
___
## Available Scripts

In the project directory, you can run:

### `yarn install`

### `yarn dev`

Runs the app in the development mode.<br>
Open [http://localhost:4000](http://localhost:4000) to view it in the browser.

